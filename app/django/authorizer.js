import Ember from 'ember';
import BaseAuthorizer from 'ember-simple-auth/authorizers/base';

export default BaseAuthorizer.extend({
	authorize(data, block) {
		const { token } = data;
		block(`Authorization`, `Token ${token}`);
	}
});