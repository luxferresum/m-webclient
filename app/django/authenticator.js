import Ember from 'ember';
import BaseAuthenticator from 'ember-simple-auth/authenticators/base';

export default BaseAuthenticator.extend({
	authenticate(username, password) {
		return new Ember.RSVP.Promise((resolve, reject) => {
			$.ajax({
				url: '/api-token-auth/',
				accepts: 'application/json',
				cache: false,
				contentType: 'application/json',
				data: JSON.stringify({username, password}),
				error: reject,
				method: 'POST',
				success: resolve
			});
		}).then(data => ({ token: data.token, username: username }));
	},
	restore(data) {
		return Ember.RSVP.resolve(data);
	},
	invalidateSession() {
		return true;
	}
})