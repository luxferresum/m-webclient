import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';

export default DS.RESTAdapter.extend(DataAdapterMixin, {
	authorizer: 'authorizer:django',
	namespace: 'api/v1',
	pathForType: function(modelName) {
		var decamelized = Ember.String.decamelize(modelName);
		return Ember.String.pluralize(decamelized) + '/';
	},
	shouldReloadAll() {
		return false;
	},
	shouldBackgroundReloadAll() {
		return true;
	}
});
