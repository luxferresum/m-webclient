import Ember from 'ember';

export default Ember.Service.extend({
	store: Ember.inject.service('store'),
	devices: Ember.computed({
		get() {
			return this.get('store').findAll('device');
		}
	})
});
